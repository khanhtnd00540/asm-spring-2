package t1708m.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sun.misc.Cleaner;
import t1708m.spring.entity.Student;
import t1708m.spring.reponsitory.StudentRepository;

import java.util.Calendar;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    public Page<Student> getList(int page, int limit){
        return studentRepository.findAll(PageRequest.of(page -1, limit));
    }

    public Student getDetail(String email){
        return studentRepository.findByEmail(email).orElse(null);
    }

    public Student login(String email, String passWord){
        // Tìm tài khoản có email trùng xem tồn tại không.
        Optional<Student> optionalAccount = studentRepository.findByEmail(email);
        if (optionalAccount.isPresent()) {
            // So sánh password xem trùng không
            Student student = optionalAccount.get();
            if (student.getPassword().equals(passwordEncoder.encode(passWord))) {
                return student;
            }
        }
        return null;
    }

    public Student register(Student student){
        //can ma hoa
        student.setPassword(passwordEncoder.encode(student.getPassword()));
        student.setCreatedAt(Calendar.getInstance().getTimeInMillis());
        student.setUpdatedAt(Calendar.getInstance().getTimeInMillis());
        return studentRepository.save(student);
    }

    public Student findByEmail(String email){
        Optional<Student> optionalStudent = studentRepository.findByEmail(email);
        return optionalStudent.orElse(null);
    }

}
